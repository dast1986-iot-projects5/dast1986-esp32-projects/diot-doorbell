#include "doorbell.h"
//------------------------------------------------------------------------------
namespace diot
{
    Doorbell::Doorbell(uint8_t serial_severity_logging /* = DEBUG */)
        : DIOT_WiFi(DIOT_APP_NAME, DIOT_APP_VERSION_MAJOR, DIOT_APP_VERSION_MINOR, DIOT_APP_VERSION_REVISION, DIOT_APP_BRANCH, 7, serial_severity_logging)
    {
        _logger->debug("Doorbell", "Constructor started");

        // Reset the counters
        __counters_presses_total = 0;
        __counters_presses = 0;
        __counters_presses_cooldown_total = 0;
        __counters_presses_cooldown = 0;
        __released_time = millis();

        // Reset the internal variabled
        __pressed = false;

        // Set a callback in the MQTT for commands
        _mqtt_client.callback_cmds = std::bind(&Doorbell::mqtt_command, this, std::placeholders::_1, std::placeholders::_2);

        _logger->debug("Doorbell", "Constructor done");
    }
    //--------------------------------------------------------------------------
    void Doorbell::mqtt_command(const char *command, const char *payload)
    {
        _logger->debug("Doorbell", "MQTT command: ", command);
        _logger->debug("Doorbell", "MQTT payload: ", payload);

        // Handle the command
        if (strcmp(command, "press_button") == 0)
        {
            // The command 'press_button' emulates a button press. If the
            // payload is empty, it will emulate a press of 500ms. If there is
            // a number in the payload, it will wait that amount of ms.

            // If there is a payload, we convert it to a number so we can use
            // it as the amount of time that the button is pressed
            uint16_t timeout = 500;
            if (strcmp(payload, "") != 0)
            {
                uint16_t timeout_temp = atoi(payload);
                if (timeout_temp > 0)
                {
                    timeout = timeout_temp;
                }
            }

            // Pretend that the button is pressed
            doorbell_press();
            delay(timeout);
            doorbell_release();
        }
        else if (strcmp(command, "doorbell") == 0)
        {
            // The command 'doorbell' can do three things:
            // - No payload:               report the status
            // - Payload: 0, off or false: disable the doorbell
            // - Payload: 1, on or true:   enable the doorbell

            if (strcmp(payload, "0") == 0 || strcmp(payload, "off") == 0 || strcmp(payload, "false") == 0)
            {
                // Disable the doorbell
                *__doorbell_enabled = false;
                setup_doorbell();
                _cfglist[__cfgblock_dbl_configuration].save_settings();
                update_mqtt();
            }
            else if (strcmp(payload, "1") == 0 || strcmp(payload, "on") == 0 || strcmp(payload, "true") == 0)
            {
                // Enable the doorbell
                *__doorbell_enabled = true;
                setup_doorbell();
                _cfglist[__cfgblock_dbl_configuration].save_settings();
                update_mqtt();
            }
            else
            {
                // Empty payload, report the status back
                update_mqtt();
            }
        }
        else if (strcmp(command, "relay") == 0)
        {
            // The command 'relay' can do three things:
            // - No payload:               report the status
            // - Payload: 0, off or false: disable the relay
            // - Payload: 1, on or true:   enable the relay

            if (strcmp(payload, "0") == 0 || strcmp(payload, "off") == 0 || strcmp(payload, "false") == 0)
            {
                // Disable the relay
                *__relay_enabled = false;
                setup_relay();
                _cfglist[__cfgblock_relay_configuration].save_settings();
                update_mqtt();
            }
            else if (strcmp(payload, "1") == 0 || strcmp(payload, "on") == 0 || strcmp(payload, "true") == 0)
            {
                // Enable the relay
                *__relay_enabled = true;
                setup_relay();
                _cfglist[__cfgblock_relay_configuration].save_settings();
                update_mqtt();
            }
            else
            {
                // Empty payload, report the status back
                update_mqtt();
            }
        }
        else if (strcmp(command, "reset_counters_since_boot") == 0)
        {
            // The command 'reset_counters_since_boot' resets the doorbell
            // counters to zero

            // Set the counters to zero
            __counters_presses = 0;
            __counters_presses_cooldown = 0;

            // Update the MQTT clients
            update_mqtt();
        }
        else if (strcmp(command, "reset_counters") == 0)
        {
            // The command 'reset_counters' resets all doorbell counters to zero

            // Set the counters to zero
            __counters_presses = 0;
            __counters_presses_cooldown = 0;
            __counters_presses_total = 0;
            __counters_presses_cooldown_total = 0;

            // Update the MQTT clients
            update_mqtt();

            // Save the settings
            _cfglist[__cfgblock_information].save_settings();
        }
    }
    //--------------------------------------------------------------------------
    void Doorbell::setup()
    {
        _logger->debug("Doorbell", "Starting setup");

        // Start the setup from the base class
        _setup();

        // Create a informationblock for this application
        __cfgblock_information = find_empty_configblock();
        _cfglist[__cfgblock_information].define("Doorbell counters", "ddbl-counters", TYPE_INFO);
        _cfglist[__cfgblock_information].settings[0].define_info_uint16("Pressed (total)", &__counters_presses_total, "presses", 0);
        _cfglist[__cfgblock_information].settings[1].define_info_uint16("Pressed (since boot)", &__counters_presses);
        _cfglist[__cfgblock_information].settings[2].define_info_uint16("During cooldown (total)", &__counters_presses_cooldown_total, "presses_cd", 0);
        _cfglist[__cfgblock_information].settings[3].define_info_uint16("During cooldown (since boot)", &__counters_presses_cooldown);

        // Create a configurationblock to configure the doorbell
        __cfgblock_dbl_configuration = find_empty_configblock();
        _cfglist[__cfgblock_dbl_configuration].define("Doorbell", "ddbl-config", TYPE_CONFIG);
        _cfglist[__cfgblock_dbl_configuration].settings[0].define_bool("enabled", "Enabled", false);
        _cfglist[__cfgblock_dbl_configuration].settings[1].define_bool("pullup", "Use pullup resistor", true);
        _cfglist[__cfgblock_dbl_configuration].settings[2].define_bool("high", "GPIO is HIGH when pressed", false);
        _cfglist[__cfgblock_dbl_configuration].settings[3].define_uint16("gpio", "GPIO for doorbell", 0);
        _cfglist[__cfgblock_dbl_configuration].settings[4].define_uint16("cooldown", "Cooldown seconds", 0);

        // Set the internal pointers for the doorbell configuration
        __doorbell_enabled = _cfglist[__cfgblock_dbl_configuration].settings[0].get_pointer_bool();
        __doorbell_pullup = _cfglist[__cfgblock_dbl_configuration].settings[1].get_pointer_bool();
        __doorbell_pressed_high = _cfglist[__cfgblock_dbl_configuration].settings[2].get_pointer_bool();
        __doorbell_gpio = _cfglist[__cfgblock_dbl_configuration].settings[3].get_pointer_uint16();
        __doorbell_cooldown = _cfglist[__cfgblock_dbl_configuration].settings[4].get_pointer_uint16();

        // Create a configurationblock to configure a optional relay
        __cfgblock_relay_configuration = find_empty_configblock();
        _cfglist[__cfgblock_relay_configuration].define("Relay (for chime)", "ddbl-relay", TYPE_CONFIG);
        _cfglist[__cfgblock_relay_configuration].settings[0].define_bool("relay", "Activate relay", false);
        _cfglist[__cfgblock_relay_configuration].settings[1].define_uint16("gpio", "GPIO for relay", 0);

        // Set the internal pointers for the relay configuration
        __relay_enabled = _cfglist[__cfgblock_relay_configuration].settings[0].get_pointer_bool();
        __relay_gpio = _cfglist[__cfgblock_relay_configuration].settings[1].get_pointer_uint16();

        // Retrieve the information. We have to do this in setup() and not in
        // theconstructor, because the _preferences library isn't working until
        // the setup() method is called
        _cfglist[__cfgblock_information].retrieve_settings();
        _cfglist[__cfgblock_dbl_configuration].retrieve_settings();
        _cfglist[__cfgblock_relay_configuration].retrieve_settings();

        // Setup the local objects
        setup_doorbell();
        setup_relay();

        // Set the default status to 'released'
        _mqtt_client.send_message(DOORBELL_MQTT_DOORBELL_TOPIC, DOORBELL_MQTT_DOORBELL_PAYLOAD_RELEASED, DOORBELL_MQTT_DOORBELL_RETAIN, false);

        // Update MQTT
        update_mqtt();

        // Turn off the led, we are done with it
        _onboard_led.off();

        _logger->debug("Doorbell", "Done with setup");
    }
    //--------------------------------------------------------------------------
    void Doorbell::loop()
    {
        // Start the loop from the base class
        if (_loop())
        {
            _logger->debug("Doorbell", "Starting loop");

            // Check if the doorbell button is pressed
            if (*__doorbell_enabled && *__doorbell_gpio > 0)
            {
                if (__doorbell && !__pressed)
                {
                    if (((float)millis() - __released_time) / 1000 >= *__doorbell_cooldown)
                    {
                        _logger->info("Doorbell", "Pressed");
                        doorbell_press();
                    }
                    else
                    {
                        _logger->info("Doorbell", "Pressed during cooldown");

                        // Set pressed to true so we can wait for the release
                        __pressed = true;

                        // Increase the counters
                        __counters_presses_cooldown_total++;
                        __counters_presses_cooldown++;
                    }
                }
                else if (!__doorbell && __pressed)
                {
                    if (((float)millis() - __released_time) / 1000 >= *__doorbell_cooldown)
                    {
                        _logger->info("Doorbell", "Released");
                        doorbell_release();
                    }
                    else
                    {
                        _logger->info("Doorbell", "Released during cooldown");

                        // Set pressed to true so we can wait for the next press
                        __pressed = false;
                    }
                }
            }

            _logger->debug("Doorbell", "Done with loop");
        }

        delay(50);
    }
    //--------------------------------------------------------------------------
    void Doorbell::setup_doorbell()
    {
        // Configure the physical doorbell
        if (*__doorbell_enabled && *__doorbell_gpio > 0)
        {
            _logger->debug("Doorbell", "Configuring doorbell");
            __doorbell.set_gpio(*__doorbell_gpio);
            __doorbell.set_pullup(*__doorbell_pullup);

            // Set the values
            if (*__doorbell_pressed_high)
            {
                __doorbell.set_values(HIGH, LOW);
            }
            else
            {
                __doorbell.set_values(LOW, HIGH);
            }
        }
    }
    //--------------------------------------------------------------------------
    void Doorbell::setup_relay()
    {
        // Configure the relay
        if (*__relay_gpio > 0)
        {
            _logger->debug("Doorbell", "Configuring relay");
            __relay.set_gpio(*__relay_gpio);

            // Turn off the relay in the rare case it is turned on
            __relay.off();
        }
    }
    //--------------------------------------------------------------------------
    void Doorbell::doorbell_press()
    {
        // Set the internal variable to 'true'
        __pressed = true;

        // Increase the counters
        __counters_presses_total++;
        __counters_presses++;

        // Save the counters
        _cfglist[__cfgblock_information].save_settings();

        // Turn on the on-board LED
        _onboard_led.on();

        // Check if we should turn on the relay
        if (*__relay_enabled && *__relay_gpio > 0)
        {
            _logger->debug("Doorbell", "Turning on relay");

            // Turn it on
            __relay.on();
        }

        // Send a message to MQTT
        _mqtt_client.send_message(DOORBELL_MQTT_DOORBELL_TOPIC, DOORBELL_MQTT_DOORBELL_PAYLOAD_PRESSED, DOORBELL_MQTT_DOORBELL_RETAIN, false);
    }
    //--------------------------------------------------------------------------
    void Doorbell::doorbell_release()
    {
        // Set the internal variable to 'false'
        __pressed = false;

        // Check if we should turn off the relay
        if (*__relay_enabled && *__relay_gpio > 0)
        {
            _logger->debug("Doorbell", "Turning off relay");

            // Turn it off
            __relay.off();
        }

        // Send a message to MQTT
        _mqtt_client.send_message(DOORBELL_MQTT_DOORBELL_TOPIC, DOORBELL_MQTT_DOORBELL_PAYLOAD_RELEASED, DOORBELL_MQTT_DOORBELL_RETAIN, false);

        // Update MQTT
        update_mqtt();

        // Turn off the on-board LED
        _onboard_led.off();

        // Set the time for the release
        __released_time = millis();
    }
    //--------------------------------------------------------------------------
    void Doorbell::update_mqtt()
    {
        // Method to update the MQTT broker with the status of the application

        // Creata a buffer for itoa
        char itoa_buffer[6];

        // Update MQTT with the correct information
        _mqtt_client.send_message(DOORBELL_MQTT_STATUS_COUNTERS_PRESSES_TOTAL, itoa(__counters_presses_total, itoa_buffer, 10), true, false);
        _mqtt_client.send_message(DOORBELL_MQTT_STATUS_COUNTERS_PRESSES, itoa(__counters_presses, itoa_buffer, 10), true, false);
        _mqtt_client.send_message(DOORBELL_MQTT_STATUS_COUNTERS_PRESSES_COOLDOWN_TOTAL, itoa(__counters_presses_cooldown_total, itoa_buffer, 10), true, false);
        _mqtt_client.send_message(DOORBELL_MQTT_STATUS_COUNTERS_PRESSES_COOLDOWN, itoa(__counters_presses_cooldown, itoa_buffer, 10), true, false);
        _mqtt_client.send_message(DOORBELL_MQTT_STATUS_DOORBELL_ENABLED, (*__doorbell_enabled) ? "enabled" : "disabled", true, false);
        _mqtt_client.send_message(DOORBELL_MQTT_STATUS_RELAY_ENABLED, (*__relay_enabled) ? "enabled" : "disabled", true, false);
    }
}