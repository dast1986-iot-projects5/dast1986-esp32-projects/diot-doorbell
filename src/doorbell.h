#ifndef _diot_doorbell_h_
#define _diot_doorbell_h_
//------------------------------------------------------------------------------
// Includes
#include <diot_wifi.h>
#include <gpio_digital_input.h>
#include <gpio_digital_output.h>
//------------------------------------------------------------------------------
// Static configuration
#define DIOT_APP_NAME "Doorbell"
#define DIOT_APP_VERSION_MAJOR 1
#define DIOT_APP_VERSION_MINOR 0
#define DIOT_APP_VERSION_REVISION 0
#define DIOT_APP_BRANCH "Production"

#ifndef SERIAL_LOGLEVEL
#define SERIAL_LOGLEVEL WARNING
#endif
#define DOORBELL_MQTT_DOORBELL_TOPIC "status/doorbell_pressed"
#define DOORBELL_MQTT_DOORBELL_PAYLOAD_PRESSED "pressed"
#define DOORBELL_MQTT_DOORBELL_PAYLOAD_RELEASED "released"
#define DOORBELL_MQTT_DOORBELL_RETAIN true
#define DOORBELL_MQTT_STATUS_COUNTERS_PRESSES_TOTAL "status/pressed_total"
#define DOORBELL_MQTT_STATUS_COUNTERS_PRESSES "status/pressed"
#define DOORBELL_MQTT_STATUS_COUNTERS_PRESSES_COOLDOWN_TOTAL "status/pressed_cooldown_total"
#define DOORBELL_MQTT_STATUS_COUNTERS_PRESSES_COOLDOWN "status/pressed_cooldown"
#define DOORBELL_MQTT_STATUS_DOORBELL_ENABLED "status/doorbell_enabled"
#define DOORBELL_MQTT_STATUS_RELAY_ENABLED "status/relay_enabled"
//------------------------------------------------------------------------------
namespace diot
{
    class Doorbell : public DIOT_WiFi
    {
    private:
        // Configblock numbers
        int16_t __cfgblock_information;
        int16_t __cfgblock_dbl_configuration;
        int16_t __cfgblock_relay_configuration;

        // Doorbell counters
        uint16_t __counters_presses_total;
        uint16_t __counters_presses;
        uint16_t __counters_presses_cooldown_total;
        uint16_t __counters_presses_cooldown;

        // Doorbell configuration
        bool *__doorbell_enabled;
        bool *__doorbell_pullup;
        bool *__doorbell_pressed_high;
        uint16_t *__doorbell_gpio;
        uint16_t *__doorbell_cooldown;

        // Relay configuration
        bool *__relay_enabled;
        uint16_t *__relay_gpio;

        // Objects for peripherals
        GPIO_Digital_Input __doorbell;
        GPIO_Digital_Output __relay;

        // Internal variables
        bool __pressed;
        unsigned long __released_time;

    public:
        // Constructors and destructors
        Doorbell(uint8_t serial_severity_logging = DEBUG);

        // Methods for commands
        void mqtt_command(const char *command, const char *payload);

        // Methods for the ESP board
        void setup();
        void loop();

        // Methods to setup objects
        void setup_doorbell();
        void setup_relay();

        // Methods for the doorbell
        void doorbell_press();
        void doorbell_release();

        // Methods to update the status
        void update_mqtt();
    };
}
//------------------------------------------------------------------------------
#endif